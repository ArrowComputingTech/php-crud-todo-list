<?php include './db.php'; 
  $page = (isset($_GET['page']) ? (int)$_GET['page'] : 1);
  $perPage = (isset($_GET['per-page']) && (int)$_GET['per-page'] <= 20 ? (int)$_GET['per-page'] : 10 );
  $start = ($page > 1) ? (($page - 1) * $perPage) : 0;
  $tasks = "select * from tasks limit ".$start." , ".$perPage." ";
  $total = $db->query("select * from tasks")->num_rows;
  $pages = ceil($total / $perPage);
  $rows = $db->query($tasks);
?>
<html lang="en">
  <head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <meta charset="utf-8">
    <title>CRUDdy Todo List</title>
    <meta name="description" content="Basic PHP CRUD app">
    <meta name="author" content="joshmcfarland">
    <link rel="stylesheet" href="css/styles.css?v=1.0">
  </head>

  <body>
    <div class="container">
      <center><h1>TODO LIST</h1></center>
      <div class="rows">
        <div class="col-md-10 col-md-offset-1">
          <button type="button" data-target="#myModal" data-toggle="modal" class='btn btn-primary'>Add task</button>
          <!--<button type="button" class='btn btn-warning pull-right .dropleft' onclick='undo()'>Undo</button> -->
          <hr>
          <div class='text-left'>
            <ol class="pagination">
              <?php for($i = 1; $i <= $pages; $i++): ?>
              <li><a href="?page=<?php echo $i; ?>&per-page=<?php echo $perPage; ?>"><?php echo $i; ?></a>
              </li>
              <?php endfor; ?>
            </ol>
          </div>
          <!-- Modal -->
          <div id='myModal' class='modal fade' role='dialog'>
            <div class='modal-dialog'>
              <!-- Modal content -->
              <div class='modal-content'>
                <div class='modal-header'>
                  <button type='button' class='close' data-dismiss='modal'>
                    &times;</button>
                  <h4 class='modal-title'>Add Task</h4>
                </div>
                <div class='modal-body'>
                  <form method='post' action='add.php'>
                    <div class='form-group'>
                      <label>Task Name</label>
                      <input type='text' required name='task' class='form-control'>
                    </div>
                    <input type='submit' name='send' value='Add Task' class='btn btn-success'>
                  </form>
                </div>
                <div class='modal-footer'>
                  <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                </div>
              </div>
            </div>
          </div>
          <table class='table table-hover'>
            <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">Task</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <?php while($row=$rows->fetch_assoc()): ?>
                <th><?php echo $row['id'] ?></th>
                <td class='col-md-10'><?php echo $row['name'] ?></td>
                <td><a href="update.php?id=<?php echo $row['id'];?>"class='btn btn-primary'>Edit</a></td>
                <td><a href="delete.php?id=<?php echo $row['id'];?>" class='btn btn-danger'>Delete</a></td>
              </tr>
              <?php endwhile; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </body>
</html>
