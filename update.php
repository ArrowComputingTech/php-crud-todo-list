<!DOCTYPE html>
<?php include './db.php'; 
  $id = (int)$_GET['id'];
  $getIDs = "select * from tasks where id = '$id'";
  $rows = $db->query($getIDs);
  $row = $rows->fetch_assoc();
  if(isset($_POST['send'])) {
    $task = htmlspecialchars($_POST['task']);
    $updateTasks = "update tasks set name='$task' where id='$id'";
    $db->query($updateTasks);
    header('location: index.php');
  }
  if(isset($_POST['cancel'])) {
    header('location: index.php');
  }
?>
<html lang="en">
  <head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <meta charset="utf-8">
    <title>CRUDding along</title>
    <meta name="description" content="Basic PHP CRUD app">
    <meta name="author" content="joshmcfarland">
    <link rel="stylesheet" href="css/styles.css?v=1.0">
  </head>

  <body>
    <div class="container">
      <center><h1>Update todo list</h1></center>
      <div class="rows">
        <div class="col-md-10 col-md-offset-1">
          <hr>
          <form method='post'>
            <div class='form-group'>
              <label>Task Name</label>
              <input type='text' required name='task' value="<?php echo $row['name'];?>" class='form-control'>
            </div>
            <input type='submit' name='send' value="Update Task" class='btn btn-success'>
            <input type='submit' name='cancel' value="Cancel" class='btn btn-danger'>
          </form>
        </div>
      </div>
    </div>
  </body>
</html>
